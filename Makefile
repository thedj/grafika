all:
	gcc -Iinclude/ src/callbacks.c src/camera.c src/init.c src/main.c src/myobject.c src/scene.c src/texture.c src/earth.c src/mars.c src/moon.c src/utils.c src/collisionDetection.c -lSOIL -lobj -lopengl32 -lglu32 -lglut32 -lm -o beadando.exe -Wall -Wextra -Wpedantic -std=c99

linux:
	gcc -Iinclude/ src/callbacks.c src/camera.c src/init.c src/main.c src/myobject.c src/scene.c src/texture.c src/earth.c src/mars.c src/moon.c src/utils.c src/collisionDetection.c -lSOIL -lobj -lGL -lGLU -lglut -lm -o beadando -Wall -Wextra -Wpedantic -std=c99
