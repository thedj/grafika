#ifndef COLLISION_H
#define COLLISION_H

/*
* Manages the collisions between the earth and the camera.
*/
void earth_camera_collision(Earth* earth, Camera* camera);


/*
* Manages the collisions between the moon and the camera.
*/
void moon_camera_collision(Moon* moon, Camera* camera);

/*
*Manages the collisions between the mars and imagined borders
*/
void mars_room_collision(Mars* mars);


/*
*Manages the collisions between the moon and the wall
*/
void moon_room_collision(Moon* moon);

/*
*Manages the collisions between 2 moons
*/
void moon_moon_collision(Moon* moon_1, Moon* moon_2);


/*
*Manages the collisions between the moon and the earth
*/
void moon_earth_collision(Moon* moon,Earth* earth);

#endif /* COLLISION_H_H */