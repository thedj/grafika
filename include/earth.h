#ifndef EARTH_H
#define EARTH_H

#include "camera.h"
#include "texture.h"
#include "myobject.h"
#include "utils.h"

#include <obj/model.h>


typedef struct Earth
{
   Object object;
} Earth;

/*
* Moves the earth in a fixed pattern.
*/
void move_earth(Earth* earth, double time);



#endif /* EARTH_H */