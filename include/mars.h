#ifndef MARS_H
#define MARS_H

#include "camera.h"
#include "texture.h"
#include "myobject.h"
#include "utils.h"

#include <obj/model.h>


typedef struct Mars
{
   Object object;
   double direction;
   double secondaryDirection;
} Mars;

/*
* Moves the mars in a fixed pattern.
*/
void move_mars(Mars* mars,double time);

/*
* Sets the direction of the planet.
*/
void init_mars_direction(Mars* mars,double direction);

#endif /* MARS_H */