#ifndef MOON_H
#define MOON_H

#include "camera.h"
#include "texture.h"
#include "myobject.h"
#include "utils.h"

#include <obj/model.h>


typedef struct Moon
{
   Object object;
   vec3 speed;
} Moon;

/*
* Moves the moon according to its speed.
*/
void move_moon(Moon* moon,double time);

/*
* Sets the direction of the moon randomly.
*/
void init_moon_direction(Moon* moon);

#endif /* MOON_H */