#ifndef MYOBJECT_H
#define MYOBJECT_H

#include "camera.h"
#include "texture.h"

#include <obj/model.h>

typedef struct Object
{
    Model objectModel;
    Material objectMaterial;
    GLuint objectTextureId;
	vec3 position;
	double size;
} Object;


/**
* Initalizes the object's position and size
*/
void init_object(Object* object,double x, double y, double z, double size);

#endif /* MYOBJECT_H */