#ifndef SCENE_H
#define SCENE_H

#include "camera.h"
#include "texture.h"
#include "myobject.h"
#include "earth.h"
#include "mars.h"
#include "moon.h"

#include <obj/model.h>

typedef struct Scene
{
    Object cube;
    Earth earth;
	Mars mars;
	Moon moon[20];
	
	GLuint helpTextureId;
	float ambientLight[4];
	float diffuseLight[4];
	float specularLight[4];
	
} Scene;

/**
 * Initialize the scene by loading models.
 */
void init_scene(Scene* scene);

/*
* Sets the lightings other than ambient.
*/
void set_not_ambient_lighting();

/**
 * Set the lighting of the scene.
 */
void set_lighting(Scene* scene);

/**
 * Set the current material.
 */
void set_material(const Material* material);

/**
 * Draw the scene objects.
 */
void draw_scene(Scene* scene);

/*
* Changes the lighting.
*/
void change_lighting(Scene* scene,int amount);

#endif /* SCENE_H */
