#include "callbacks.h"
#include "collisionDetection.h"

#include <obj/load.h>

#define VIEWPORT_RATIO (16.0 / 9.0)
#define VIEWPORT_ASPECT 50.0

struct {
    int x;
    int y;
} mouse_position;

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();
    set_view(&camera);
    draw_scene(&scene);
    glPopMatrix();
	
    if (is_help_visible) {
        show_help(scene.helpTextureId);
    }

    glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height)
{
    int x, y, w, h;
    double ratio;

    ratio = (double)width / height;
    if (ratio > VIEWPORT_RATIO) {
        w = (int)((double)height * VIEWPORT_RATIO);
        h = height;
        x = (width - w) / 2;
        y = 0;
    }
    else {
        w = width;
        h = (int)((double)width / VIEWPORT_RATIO);
        x = 0;
        y = (height - h) / 2;
    }

    glViewport(x, y, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(VIEWPORT_ASPECT, VIEWPORT_RATIO, 0.01, 10000.0);
}

void mouse(int button, int state, int x, int y)
{
    mouse_position.x = x;
    mouse_position.y = y;
	(void)state;
	(void)button;
}

void motion(int x, int y)
{
    rotate_camera(&camera, mouse_position.x - x, mouse_position.y - y);
    mouse_position.x = x;
    mouse_position.y = y;
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case 'w':
        set_camera_speed(&camera, 1);
        break;
    case 's':
        set_camera_speed(&camera, -1);
        break;
    case 'a':
        set_camera_side_speed(&camera, 1);
        break;
    case 'd':
        set_camera_side_speed(&camera, -1);
        break;
	case 32 :
		set_camera_vertical_speed(&camera,1);
		break;
	case 'c' :
		set_camera_vertical_speed(&camera,-1);
		break;	
	case '+':
		change_lighting(&scene,1);
		break;
	case '-':
		change_lighting(&scene,-1);
		break;
    }
	(void)x;
	(void)y;
    glutPostRedisplay();
}

void keyboard_up(unsigned char key, int x, int y)
{
    switch (key) {
    case 'w':
    case 's':
        set_camera_speed(&camera, 0.0);
        break;
    case 'a':
    case 'd':
        set_camera_side_speed(&camera, 0.0);
        break;
	case 32:
	case 'c':
		set_camera_vertical_speed(&camera,0.0);
		break;
	case '+':
		
		break;
	case '-':
		
		break;
    }
	(void)x;
	(void)y;

    glutPostRedisplay();
}

void specialFunc(int key, int x, int y) {
	switch (key)
	{
		case GLUT_KEY_F1:
			
			if (is_help_visible) 
			{
				is_help_visible = FALSE;
			}else 
			{
				is_help_visible = TRUE;
			}
			break;
	}
	(void)x;
	(void)y;
	glutPostRedisplay();	
}

void idle()
{
    static int last_frame_time = 0;
    int current_time;
    double elapsed_time;
   
    current_time = glutGet(GLUT_ELAPSED_TIME);
    elapsed_time = (double)(current_time - last_frame_time) / 1000;
    last_frame_time = current_time;
	
	
	//movements
	move_earth(&(scene.earth),elapsed_time);
	move_mars(&(scene.mars),elapsed_time);
	
	if(elapsed_time<2.0)
	{
		for(int i=0;i<20;i++)
		{
			move_moon(&(scene.moon[i]),elapsed_time);
		}
	}
	
	//collisions
	
	earth_camera_collision(&(scene.earth),&camera);
	
	for(int i=0;i<20;i++)
	{
		moon_camera_collision(&(scene.moon[i]),&camera);
		moon_room_collision(&(scene.moon[i]));
		moon_earth_collision(&(scene.moon[i]),&(scene.earth));
		for(int j=0;j<20;j++)
		{
			if(i!=j)
			{
				moon_moon_collision(&(scene.moon[i]),&(scene.moon[j]));
			}
		}
	}
	
	
	mars_room_collision(&(scene.mars));
	
    update_camera(&camera, elapsed_time);
	
    glutPostRedisplay();
}
