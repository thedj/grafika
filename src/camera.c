#include "camera.h"
#include "scene.h"
#include "earth.h"

#include <GL/glut.h>

#include <math.h>


#define OUTERWALL 5.8


void init_camera(Camera* camera)
{
    camera->position.x = 4.5;
    camera->position.y = 4.5;
    camera->position.z = 4.5;
    camera->rotation.x = 340.0;
    camera->rotation.y = 0.0;
    camera->rotation.z = 220.0;
    camera->speed.x = 0.0;
    camera->speed.y = 0.0;
    camera->speed.z = 0.0;

    is_help_visible = FALSE;
}

void update_camera(Camera* camera, double time)
{
	camera->prevPosition=camera->position;
    double angle;
    double side_angle;
	double verticalAngle;

    angle = degree_to_radian(camera->rotation.z);
    side_angle = degree_to_radian(camera->rotation.z + 90.0);
	verticalAngle=degree_to_radian(camera->rotation.x);

    camera->position.x += cos(angle) * camera->speed.y * time;
    camera->position.y += sin(angle) * camera->speed.y * time;
    camera->position.z += sin(verticalAngle) * camera->speed.y * time;
	
    camera->position.x += cos(side_angle) * camera->speed.x * time;
    camera->position.y += sin(side_angle) * camera->speed.x * time;
	
	camera->position.z += camera->speed.z * time;
	
	
	can_move(camera);
	
    
}

void set_view(const Camera* camera)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(-(camera->rotation.x + 90), 1.0, 0, 0);
    glRotatef(-(camera->rotation.z - 90), 0, 0, 1.0);
    glTranslatef(-camera->position.x, -camera->position.y, -camera->position.z);
}

void rotate_camera(struct Camera* camera, double horizontal, double vertical)
{
	double fallbackrotationOfX;

	// Vertical, with rollover protection
	if(camera->rotation.x >= 0 && camera->rotation.x <= 90)
	{
		fallbackrotationOfX = 90;
	}else
	{
		fallbackrotationOfX = 270;
	}

	if(camera->rotation.x + vertical > 90 && camera->rotation.x + vertical < 270)
	{
		camera->rotation.x = fallbackrotationOfX;
	}else 
	{
		camera->rotation.x += vertical;
	}

	if(camera->rotation.x  > 90 && camera->rotation.x < 270)
	{
		set_clear_camera_rotation(camera);
	}


	// Horizontal
	camera->rotation.z += horizontal;

	if (camera->rotation.z < 0) {
		camera->rotation.z += 360.0;
	}

	if (camera->rotation.z > 360.0) {
		camera->rotation.z -= 360.0;
	}

	if (camera->rotation.x < 0) {
		camera->rotation.x += 360.0;
	}

	if (camera->rotation.x > 360.0) {
		camera->rotation.x -= 360.0;
	}
	
}

void set_clear_camera_rotation(Camera* camera)
{
	camera->rotation.x = 0;
}


void set_camera_speed(Camera* camera, double speed)
{
    camera->speed.y = speed;
}

void set_camera_side_speed(Camera* camera, double speed)
{
    camera->speed.x = speed;
}

void set_camera_vertical_speed(Camera* camera, double speed)
{
    camera->speed.z = speed;
}

void show_help(int texture)
{
	
    glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glColor3f(1, 1, 1);
	glBindTexture(GL_TEXTURE_2D, texture);
    glBegin(GL_QUADS);
	
    glTexCoord2f(0, 0);
    glVertex3f(-1.7, 1, -2);
	
    glTexCoord2f(1, 0);
    glVertex3f(1.7, 1, -2);
	
    glTexCoord2f(1, 1);
    glVertex3f(1.7, -1, -2);
	
    glTexCoord2f(0, 1);
    glVertex3f(-1.7, -1, -2);
    glEnd();

	
    glEnable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	
}


void can_move(Camera* camera)
{
	if(camera->position.x<OUTERWALL && camera->position.y<OUTERWALL && camera->position.z<OUTERWALL && camera->position.x>-OUTERWALL && camera->position.y>-OUTERWALL && camera->position.z>-OUTERWALL)
	{
		return;
	}else
	{
		camera->position=camera->prevPosition;
	}
	
	
}








