#include "earth.h"
#include "mars.h"
#include "moon.h"
#include "camera.h"
#include <GL/glut.h>

#include <obj/load.h>
#include <obj/draw.h>

#include "camera.h"
#include "utils.h"

#define OUTERWALLLL 4.55



void earth_camera_collision(Earth* earth, Camera* camera)
{
	double distance = get_distance(earth->object.position,camera->position);
	if(distance<=1.5)
	{	if(earth->object.position.x-camera->position.x>0)
		{
			camera->position.x-=0.1;
		}else
		{
			camera->position.x+=0.1;
		}
		
		if(earth->object.position.y-camera->position.y>0)
		{
			camera->position.y-=0.1;
		}else
		{
			camera->position.y+=0.1;
		}
		
		if(earth->object.position.z-camera->position.z>0)
		{
			camera->position.z-=0.1;
		}else
		{
			camera->position.z+=0.1;
		}
	}
	
}

void moon_camera_collision(Moon* moon, Camera* camera)
{
	double distance = get_distance(moon->object.position,camera->position);
	if(distance<=0.5)
	{	if(moon->object.position.x-camera->position.x>0)
		{
			camera->position.x-=0.1;
		}else
		{
			camera->position.x+=0.1;
		}
		
		if(moon->object.position.y-camera->position.y>0)
		{
			camera->position.y-=0.1;
		}else
		{
			camera->position.y+=0.1;
		}
		
		if(moon->object.position.z-camera->position.z>0)
		{
			camera->position.z-=0.1;
		}else
		{
			camera->position.z+=0.1;
		}
	}
	
	
	
}


void mars_room_collision(Mars* mars)
{
	if(mars->object.position.z>OUTERWALLLL)
	{
		mars->direction=-90;
	}
	
	if(mars->object.position.z<-OUTERWALLLL)
	{
		mars->direction=90;
	}
}


void moon_room_collision(Moon* moon)
{
	if(moon->object.position.z>OUTERWALLLL+1.1 || moon->object.position.z<-OUTERWALLLL-1.1)
	{
		moon->speed.z*=-1;
	}
	
	if(moon->object.position.x>OUTERWALLLL+1.1 || moon->object.position.x<-OUTERWALLLL-1.1)
	{
		moon->speed.x*=-1;
	}
	
	if(moon->object.position.y>OUTERWALLLL+1.1 || moon->object.position.y<-OUTERWALLLL-1.1)
	{
		moon->speed.y*=-1;
	}
	
	
}


void moon_moon_collision(Moon* moon_1, Moon* moon_2)
{
	double distance = get_distance(moon_1->object.position,moon_2->object.position);
	if(distance<=0.2)
	{
		moon_1->speed.z*=-1;
		moon_1->speed.x*=-1;
		moon_1->speed.y*=-1;
		
		moon_1->speed.x+=rand()%120-60;
		moon_1->speed.y+=rand()%120-60;
		moon_1->speed.z+=rand()%120-60;
		
		moon_2->speed.z*=-1;
		moon_2->speed.x*=-1;
		moon_2->speed.y*=-1;
		
		moon_2->speed.x+=rand()%120-60;
		moon_2->speed.y+=rand()%120-60;
		moon_2->speed.z+=rand()%120-60;
		
	}
	

}

void moon_earth_collision(Moon* moon,Earth* earth)
{
	double distance = get_distance(earth->object.position,moon->object.position);
	if(distance<=1.3)
	{
		if(moon->object.position.x-earth->object.position.x>0)
		{
			moon->object.position.x+=0.1;
		}else
		{
			moon->object.position.x-=0.1;
		}
		
		if(moon->object.position.y-earth->object.position.y>0)
		{
			moon->object.position.y+=0.1;
		}else
		{
			moon->object.position.y-=0.1;
		}
		
		if(moon->object.position.z-earth->object.position.z>0)
		{
			moon->object.position.z+=0.1;
		}else
		{
			moon->object.position.z-=0.11;
		}
		
		
		moon->speed.z*=-1;
		moon->speed.x*=-1;
		moon->speed.y*=-1;
		
		
		moon->speed.x+=rand()%60-30;
		moon->speed.y+=rand()%60-30;
		moon->speed.z+=rand()%60-30;
	}
	
	
	
	
}








































