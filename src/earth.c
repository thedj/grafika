#include "earth.h"

#include <GL/glut.h>

#include <obj/load.h>
#include <obj/draw.h>
#include <math.h>


#include "camera.h"
#include "utils.h"


double moveCoefficient=1;
 
void move_earth(Earth* earth, double time)
{
		double angle=degree_to_radian(moveCoefficient);
		earth->object.position.x+=cos(angle)*0.6*time;
		earth->object.position.z+=sin(angle)*0.6*time;
		earth->object.position.y+=sin(angle)*0.6*time;
		moveCoefficient+=20*time;
}