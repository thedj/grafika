#include "mars.h"

#include <GL/glut.h>

#include <obj/load.h>
#include <obj/draw.h>
#include <math.h>


#include "camera.h"
#include "utils.h"



void move_mars(Mars* mars,double time)
{
	double angle=degree_to_radian(mars->direction);
	double secondaryAngle=degree_to_radian(mars->secondaryDirection);
	mars->object.position.z+=sin(angle)*0.5*time;
	mars->object.position.x+=sin(secondaryAngle)*0.8*time;
	mars->secondaryDirection+=23*time;
	
}


void init_mars_direction(Mars* mars,double initDirection)
{
	mars->direction=initDirection;
	mars->secondaryDirection=initDirection;
}