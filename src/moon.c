#include "moon.h"

#include <GL/glut.h>

#include <obj/load.h>
#include <obj/draw.h>
#include <math.h>
#include <time.h>

#include "camera.h"
#include "utils.h"



void move_moon(Moon* moon,double time)
{
	double xSpeed=degree_to_radian(moon->speed.x);
	double ySpeed=degree_to_radian(moon->speed.y);
	double zSpeed=degree_to_radian(moon->speed.z);
	
	moon->object.position.z+=sin(zSpeed)*time;
	moon->object.position.x+=sin(xSpeed)*time;
	moon->object.position.y+=sin(ySpeed)*time;
	
}


void init_moon_direction(Moon* moon)
{
	
	moon->speed.x=rand()%300-150;
	moon->speed.y=rand()%300-150;
	moon->speed.z=rand()%300-150;
	
	
}