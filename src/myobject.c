#include "myobject.h"

#include <GL/glut.h>

#include <obj/load.h>
#include <obj/draw.h>


void init_object(Object* object,double x, double y, double z, double size)
{
	object->position.x=x;
	object->position.y=y;
	object->position.z=z;
	object->size=size;
}