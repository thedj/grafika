#include "scene.h"
#include "myobject.h"
#include "earth.h"
#include <GL/glut.h>
#include <time.h>
#include <obj/load.h>
#include <obj/draw.h>

void init_scene(Scene* scene)
{
	srand(time(0));
	scene->helpTextureId = load_texture("textures//newhelp.png");
	
    load_model(&(scene->cube.objectModel), "models//room.obj");
    scene->cube.objectTextureId = load_texture("textures//room.png"); 
	
   
	
	
	load_model(&(scene->earth.object.objectModel), "models//planet2.obj");
    scene->earth.object.objectTextureId = load_texture("textures//earth.png"); 
	init_object(&(scene->earth.object),0.0,0.0,0.0,0.2);
    
	
	
	load_model(&(scene->mars.object.objectModel), "models//planet2.obj");
    scene->mars.object.objectTextureId = load_texture("textures//mars.png"); 
	init_object(&(scene->mars.object),0.0,-3.0,-3.0,3);
	init_mars_direction(&(scene->mars),90);
	
	int i;
	for(i=0;i<20;i++)
	{
		
		load_model(&(scene->moon[i].object.objectModel), "models//planet2.obj");
		scene->moon[i].object.objectTextureId = load_texture("textures//moon.jpg"); 
		init_object(&(scene->moon[i].object),rand()%8-4,rand()%8-4,rand()%8-4,0.05);
		init_moon_direction(&(scene->moon[i]));
	}
	
	
	
	
	
    scene->cube.objectMaterial.ambient.red = 0.3;
    scene->cube.objectMaterial.ambient.green = 0.3;
    scene->cube.objectMaterial.ambient.blue = 0.3;
		   
    scene->cube.objectMaterial.diffuse.red = 0.7;
    scene->cube.objectMaterial.diffuse.green = 0.7;
    scene->cube.objectMaterial.diffuse.blue = 0.7;
		   
    scene->cube.objectMaterial.specular.red = 1.0;
    scene->cube.objectMaterial.specular.green = 1.0;
    scene->cube.objectMaterial.specular.blue = 1.0;
		   
    scene->cube.objectMaterial.shininess = 0.8;
	
	float position[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, position);
	
	scene->ambientLight[0]=0.7f;
	scene->ambientLight[1]=0.7f;
	scene->ambientLight[2]=0.7f;
	scene->ambientLight[3]=0.7f;
	
	scene->diffuseLight[0]=0.5f;
	scene->diffuseLight[1]=0.5f;
	scene->diffuseLight[2]=0.5f;
	scene->diffuseLight[3]=0.5f;
	
	scene->specularLight[0]=1.0f;
	scene->specularLight[1]=1.0f;
	scene->specularLight[2]=1.0f;
	scene->specularLight[3]=1.0f;
	
	set_lighting(scene);
	
	
}



void set_lighting(Scene* scene)
{	

	glLightfv(GL_LIGHT0, GL_AMBIENT, scene->ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, scene->diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, scene->specularLight);
	float position[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, position);
  
}

void set_not_ambient_lighting()
{	
	
    
    //float diffuse_light[] = { 0.5f, 0.5f, 0.0, 0.0f };
    //float specular_light[] = { 1.0f, 1.0f, 1.0f, 0.0f };
    //float position[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	

    
    //glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
    //glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
    //glLightfv(GL_LIGHT0, GL_POSITION, position);
	
}

void set_material(const Material* material)
{
    float ambient_material_color[] = {
        material->ambient.red,
        material->ambient.green,
        material->ambient.blue
    };

    float diffuse_material_color[] = {
        material->diffuse.red,
        material->diffuse.green,
        material->diffuse.blue
    };

    float specular_material_color[] = {
        material->specular.red,
        material->specular.green,
        material->specular.blue
    };

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_material_color);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &(material->shininess));
}

void draw_scene(Scene* scene)
{
	set_lighting(scene);
	
	glDisable(GL_CULL_FACE);
    set_material(&(scene->cube.objectMaterial));
    
	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		
		glTranslatef(scene->mars.object.position.x,scene->mars.object.position.y,scene->mars.object.position.z);
		glScaled(scene->mars.object.size,scene->mars.object.size,scene->mars.object.size);
		
		
		
		glBindTexture(GL_TEXTURE_2D, scene->mars.object.objectTextureId);
		draw_model(&(scene->mars.object.objectModel));
		glBindTexture(GL_TEXTURE_2D, 0);
		
		
	glPopMatrix();
	
	
	glPushMatrix();
		glScaled(12.0,12.0,12.0);
		glBindTexture(GL_TEXTURE_2D, scene->cube.objectTextureId);
		
		glEnable( GL_BLEND );
		glBlendFunc (GL_ONE, GL_ONE);
		
		draw_model(&(scene->cube.objectModel));
		glBindTexture(GL_TEXTURE_2D, 0);
		
		glDisable( GL_BLEND);
		
	glPopMatrix();
	
	glDisable(GL_TEXTURE_2D); 
	
	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		
		glTranslatef(scene->earth.object.position.x,scene->earth.object.position.y,scene->earth.object.position.z);
		glScaled(scene->earth.object.size,scene->earth.object.size,scene->earth.object.size);
		
		
		glBindTexture(GL_TEXTURE_2D, scene->earth.object.objectTextureId);
		draw_model(&(scene->earth.object.objectModel));
		glBindTexture(GL_TEXTURE_2D, 0);
		
		
	glPopMatrix();
	
	glDisable(GL_TEXTURE_2D); 
	
	
	int i;
	for(i=0;i<20;i++){
	
		glPushMatrix();
			glEnable(GL_TEXTURE_2D);
			
			glTranslatef(scene->moon[i].object.position.x,scene->moon[i].object.position.y,scene->moon[i].object.position.z);
			glScaled(scene->moon[i].object.size,scene->moon[i].object.size,scene->moon[i].object.size);
			
			
			glBindTexture(GL_TEXTURE_2D, scene->moon[i].object.objectTextureId);
			draw_model(&(scene->moon[i].object.objectModel));
			glBindTexture(GL_TEXTURE_2D, 0);
			
			
		glPopMatrix();
	
	}
	
	glDisable(GL_TEXTURE_2D); 
	
	
	
	
	
	
		
}

void change_lighting(Scene* scene,int amount)
{
	if (scene->ambientLight[0] < 1 && amount==1)
	{
		//scene->ambientLight[0] = scene->ambientLight[1] = scene->ambientLight[2] += 0.1;	
		scene->diffuseLight[0] = scene->diffuseLight[1] = scene->diffuseLight[2] += 0.1;	
		//scene->specularLight[0] = scene->specularLight[1] = scene->specularLight[2] += 0.1;	
		
	}
	
	if (scene->ambientLight[0] > 0 && amount==-1)
	{
		//scene->ambientLight[0] = scene->ambientLight[1] = scene->ambientLight[2] -= 0.1;	
		scene->diffuseLight[0] = scene->diffuseLight[1] = scene->diffuseLight[2] -= 0.1;	
		//scene->specularLight[0] = scene->specularLight[1] = scene->specularLight[2] -= 0.1;	
	}
	
	
	
}




















