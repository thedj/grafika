#include "utils.h"

#include <math.h>

#define M_PI 3.14

double degree_to_radian(double degree)
{
	return degree * M_PI / 180.0;
}

double get_distance(vec3 position,vec3 otherPosition)
{
	double x = pow(position.x-otherPosition.x,2);
	double y = pow(position.y-otherPosition.y,2);
	double z = pow(position.z-otherPosition.z,2);
	
	return sqrt(x+y+z);
}

